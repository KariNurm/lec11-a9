import express, {Request, Response} from 'express';
import { validator } from './middlewares';
const routes = express.Router();

interface Event {
	id: string,
	title: string,
	description: string
	date: string,
	time?: string,
}

const eventList: Event[] = [];

// GET events
routes.get('/', (_req: Request, res: Response) => {
	res.json(eventList);
});

// GET events next month
routes.get('/:monthNumber', (req: Request, res: Response) => {
	const month = req.params.monthNumber;

	const filteredEvents = eventList.filter(event => new Date(event.date).getMonth() + 1 === Number(month));

	res.json(filteredEvents);
});


// POST event to claendar
routes.post('/', validator, (req: Request, res: Response) => {
	const {id, title, description, date, time} = req.body;
	
	if(eventList.find(event => event.id === id)) {
		return res.status(400).send('Id in use');
	}

	const newEvent = {
		id: id,
		title: title,
		description: description,
		date: date,
		time: time
	};

	eventList.push(newEvent);

	res.status(201).send('Event added!');
});


//PUT modify the event
routes.put('/:eventId', validator, (req:Request, res: Response) => {
	const {id, title, description, date, time} = req.body;
	const eventId = req.params.eventId;

	const findIndex = eventList.findIndex(event => event.id === eventId);
	if(findIndex === -1) {
		return res.status(400).send('No such event');
	}
	else if(eventList.find(event => event.id === id)) {
		return res.status(400).send('Id in use');
	}

	const newEvent = {
		id: id,
		title: title,
		description: description,
		date: date,
		time: time
	};

	eventList[findIndex] = newEvent;

	res.send('Event modified!');

});


// DELETE event
routes.delete('/:eventId', (req: Request, res: Response) => {
	const eventId = req.params.eventId;
	const findIndex = eventList.findIndex(event => event.id === eventId);
	if(findIndex === -1) {
		return res.status(400).send('No such event');
	}
	
	eventList.splice(findIndex, 1);
	res.status(201).send('Event removed');
});


export default routes;