import express from 'express';
import routes from './routes';
import {logger, notFound} from './middlewares';


const server = express();
server.use(express.json());
server.use(logger);

server.use('/api/calendar', routes);

server.use(notFound);

server.listen(process.env.PORT ?? 3000, () => {
	console.log('server operational');
});