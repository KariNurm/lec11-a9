import { Request, Response, NextFunction } from 'express';

const notFound = (_req: Request, res: Response) => {
	res.status(404).send();
};


const logger = (req: Request, res: Response, next: NextFunction) => {
	console.log({
		date: new Date().toISOString(),
		method: req.method,
		url: req.url,
		body: req.body,
	});
	next();
};

const validator = (req: Request, res: Response, next: NextFunction) => {
	const event = req.body;
	const requiredParams = ['id', 'title', 'description', 'date'];
	const allowedParams = [...requiredParams, 'time'];

	for(const param in event) {
		if(!allowedParams.includes(param)) {
			return res.status(400).send('Invalid input params');
		}
	}

	for(const param of requiredParams) {
		if (!(param in event)) {
			return res.status(400).send('Missing request params');
		}
	}

	next();
};



export { notFound, logger, validator};